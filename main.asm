%include "lib.inc"
%include "words.inc"
%include "dict.inc"


%define qw 8 

%define buff_size 256

section .bss                    ; buffer allocation
buff: resb buff_size

section .rodata
    length_error: db "Your string is too long", 0
    not_find_error: db "I cann't find the string(", 0

section .text

global _start

_start:
mov     rdi,  buff                      ; the start buffer adress 
mov     rsi,  buff_size                 ; the buffer size
call    read_word                       ; if we cann't read return 0, else returns the buffer address in rax, the word length in rdx
test    rax,  rax
jz              .too_long                       ; if the string is too long, go away 
mov     rdi,  buff
mov     rsi,  label
call    find_word
test    rax,  rax                       ; if it's 0 then didn't find
jz              .not_found
mov     rdi,  rax                       ; the result find_word to rdi 
add     rdi,  qw                         ; to skip key and get label
push    rdi                                     ; save data
call    string_length
pop     rdi                             ; restore value
add     rdi, rax                        ; go to the end of string
inc     rdi                             ; skip null-terminated
call    print_string
call    print_newline
xor     rdi, rdi
call    exit



.too_long:
mov     rdi, length_error
jmp     .error


.not_found:
mov     rdi, not_find_error
jmp     .error

.error:
call    print_err_string
call    print_newline
mov     rdi, 1
call    exit


