rm -rf test
mkdir test

t1="third word"
t2="bu ba"
t3="pupa"
t4="vehvdsfskfjdskfsjfsdfsdfsdfsdfsdfsdjfsdklfjsdklfjsdflksdjfklsdjflksdjflskjfsdlkjfskldfjsdfjsklfjdlfjkljfklsdjklsdjflsidhgjkdfhgkldgdfjkgjdfkgjdfgkdfjgkldfjgdkflgjdflkjdflkgjkljkljkvjdkfnjdfbbwfwfjwjfwkfjewkljfwefkjeklfjklewjfkwelfjlkwejfwelkfjewflkwejfwekfjwefkljwefklwejfwelkfjweflwejflkwejfewkfjwefjewjfkwelfjwefjweflkwejfwlekfjwifessdklfjsdlfjmdskcmdfdsfsdfsdfsdfsjleijflkdsmfsdkfjsdlkfjsdfklsddmfdsfmsdkfsdmfksdnffsdevhehvvehvhevehehevhvehv"

cd test

echo "I cann't find the string( 
second word explanation
first word explanation
Your string is too long" > ./expected


echo $t1 | ./../main &> result
echo $t2 | ./../main &>> result
echo $t3 | ./../main &>> result
echo $t4 | ./../main &>> result

echo "Data:"
echo $t1
echo $t2
echo $t3
echo $t4
echo "--------------------------------------------"
echo "Expected result:"
cat expected
echo "--------------------------------------------"
echo "Result:"
cat result
echo "--------------------------------------------"


if [ $(diff -Bw result expected | wc -c) -eq 0 ];
then
        echo "Tests passed successfully! Congratulations!"
else
        echo "The tests were not passed(("
fi

cd ..
