%define label 0     ; label to the next dictionary element
%macro colon 2
%2: dq label        ; sets a label to a new element
db %1, 0            ; null-terminated label on the content
%define label %2    ; updating the label to the next element
%endmacro
