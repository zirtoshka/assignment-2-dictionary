global exit
global string_length
global string_copy
global string_equals
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global print_string
global print_err_string

section .text

 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov  rax, 60            ; invoke 'exit' system call
    xor  rdi, rdi
    syscall
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	push rdi			; so that the data is not lost
    mov  rax, rdi		; indicator from rdi to rax
  .counter:
    cmp  byte [rdi], 0 	 
    je   .end
    inc  rdi			; inc if it's that what we need (countine)
    jmp  .counter
  .end:
    sub  rdi, rax		; to find lenght
    mov  rax, rdi		; to get answer
    pop	rdi
    ret
    

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi 			; so that the data is not lost
    call string_length	
    pop rsi 	
    mov  rdx, rax		; string length in bytes 
    mov  rax, 1			; 'write' syscall number
    mov  rdi, 1			; stdout descriptor
    syscall
    ret

print_err_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 2
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
	push	rdi			; so that the data is not lost
	mov     rax, 1      ; 'write' syscall number
	mov     rsi, rsp    ; string address
    mov     rdi, 1      ; stdout descriptor
    mov     rdx, 1      ; string length in bytes (we need character code, so it's 1 byte)
    syscall
    pop		rdi			; restore the data
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov 	rdi, 0xA	; we want get this
	call 	print_char	; use our routine
    syscall
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	
	mov		rax, rdi
	mov		rcx, 10 	; we need 10 base
	push	0 			; for null-terminated string

	.loop:
	xor		rdx, rdx 	; zeroing for correct div
	div		rcx			
	add 	dx, '0'		; get ASCII digit
	push	rdx			; add stack
	cmp		rax, 0 		; if 0 then stop (we have no digits)
	jnz		.loop

	.get_number:
	pop		rax			; get digit from stack
	cmp		al, 0 		
	je		.exit		; need stop
	mov		rdi, rax	; preparing to call a routine
	call 	print_char
	jmp		.get_number

	.exit:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:

    test 	rdi, rdi    ; for check negative flag 
    jns		.positive	; if positive skip .negative

    .negative:
    push	rdi			; save data from rdi
    mov		rdi, '-'	; to add minus in input
    call 	print_char
    pop		rdi			; return data from stack
    neg 	rdi			; 'turn it into abs'


    .positive:
    call 	print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	
	.loop:
    mov al, [rsi]
    cmp al, [rdi]		; compare symbols[i]
    jnz .not_equals		; if not equals jump to return 0=false
    inc rsi 			; do i+1 for the first string
    inc rdi 			; do i+1 for the second string
    cmp al, 0 			; if it's null-terminated 
    jnz .loop 			; continue loop
    mov rax, 1 			; the end loop, so return true=1
    ret

    .not_equals:
    mov rax, 0
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov 	rdi, 0 		; stdin descriptor  
    mov 	rax, 0 		; 'read' syscall number
    mov 	rdx, 1 		; one symbol so 1 byte
    push 	0 			; allocate space for 1 symbol
    mov 	rsi, rsp 	; to read from dedicated location in memory
    syscall
    test rax, rax
    jle      .g
    pop rax
    ret
    .g:
    add     rsp, 8
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    
    push    rdi
   
    xor     rcx, rcx
    xor     rax, rax

    .loop:
    push    rdi
    push    rsi
    push    rcx
    call    read_char
    pop     rcx
    pop     rsi
    pop     rdi
    cmp    rax, 0
    je      .stop
    cmp     rax, 0xA
    je      .stop
    mov     byte[rdi], al
    inc     rdi
    inc     rcx             ; len string +=1
    cmp     rcx, rsi        ; len_string can be more than len_buffer
    jge     .fail
    jmp     .loop


    .stop:                  ; get result
    mov     rdx, rcx
    pop     rax
    ret

    .fail:                  ; if inappropriate case
    mov     rax, 0
    pop     rdi
    ret


    

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor 	rax, rax 	; result
    xor 	rcx, rcx 	; i	= 0
    push	r15 		; save data
    push	rbx	 		; save data
    xor 	r15, r15
    mov 	rbx, 10 	; we need 10 base

    .loop:
    mov 	r15b, byte[rdi+rcx] 	; get sybmbol=string[i] from string
    cmp 	r15b, '0'
    jl	 	.end 					; if it's not digit (ASCCI<'0')
    cmp 	r15b, '9' 				
    jg 		.end 					; if it's not digit (ASCCI>'9')
    sub 	r15b, '0'
    add 	rax, r15 				; forming the result
    mul 	rbx
    inc 	rcx 					; i+=1
    jmp 	.loop

    .end:
    div 	rbx 		; because we have extra mul for the last digit
    mov 	rdx, rcx 	; curr i = len res number
    pop 	rbx
    pop 	r15
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor 	rax, rax 			; res
    mov 	al, byte[rdi]		; the first symbol
    cmp 	al, '-'				
    jz 		.negative			; if got minus
    call 	parse_uint 			
    jmp 	.end

    .negative:
    inc 	rdi
    call 	parse_uint
    inc 	rdx 				; because have one more symbol = '-'
    neg 	rax

    .end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx

    .loop:
    cmp 	rax, rdx 			; compare buffer_len and i [current index string's symbol]
    jge 	.too_many_sybmbols 	; if buffer_len < string[i]
    mov cl, byte[rdi + rax]
    cmp 	cl, 0 				; if it's end of string
    jz 		.end
    mov		qword[rsi+rax], 0	; if buferr is not clear
    mov		byte[rsi+rax], cl   ; move string[i] in buffer
    inc     rax					; i+=1
    jmp 	.loop				


    .too_many_sybmbols:
    xor 	rax, rax
    ret

    .end:
    mov qword[rsi+rax], 0 		; add for null-terminated string
    inc rax
    ret
