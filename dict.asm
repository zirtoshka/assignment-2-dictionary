%include "lib.inc"

global find_word

%define qw 8

section .text

find_word: 
    push r12
    push r13
    mov r12, rdi
    .loop:
        test rsi, rsi       ; check if dict ends
        je .not_found       ; if the end, go away
        mov r13, rsi
        add rsi, qw         ; skip key to check by label
        mov rdi, r12
        call string_equals  ; if equals return 1; else return 0
        test rax, rax       ; compare with 0
        jne .found          ; if strings are equal, go to .found
        mov rsi, [r13]      ; continue searching 
        jmp .loop
    .found:
        mov rax, r13        ; label in the dict
        pop r13
        pop r12
        ret
    .not_found:
        xor rax, rax        ; output code of the incorrect result
        pop r13
        pop r12
        ret
