ASM = nasm
ASMFLAGS = -felf64
LD = ld


%.o: %.asm
  $(ASM) $(ASMFLAGS) %.asm -o %.o

dict.o: dict.asm lib.inc
  $(ASM) $(ASMFLAGS) dict.asm -o dict.o

main.o: main.asm lib.inc dict.inc words.inc
  $(ASM) $(ASMFLAGS) main.asm -o main.o


main: main.o lib.o dict.o
  $(LD) -o main $^


.PHONY: go test

go:
  ./main
  
test:
  ./test.sh
